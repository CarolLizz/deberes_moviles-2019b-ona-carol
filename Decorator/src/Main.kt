import decorator.Llavado
import decorator.LlavadoInterno
import decorator.ServicioBasico

fun main(args: Array<String>) {
    val ServicioCarro = LlavadoInterno(Llavado(ServicioBasico()))
    ServicioCarro.ejecutarServicio()
}
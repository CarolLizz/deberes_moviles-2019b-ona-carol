package observer

interface CarroColorObserver {
    fun onCambioColor(nuevoColor: String)
}
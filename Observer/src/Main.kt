import observer.Carro
import observer.LogCarroColorObserver

fun main(args: Array<String>) {
    val carro = Carro()

    carro.observer = LogCarroColorObserver()
    carro.color = "Rojo"
    carro.color = "Verde"
    carro.color = "Amarrillo"
}
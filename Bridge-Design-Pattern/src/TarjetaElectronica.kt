class TarjetaElectronica {
    var mecanismo:Mecanismo?

    constructor(mecanismo:Mecanismo){
        this.mecanismo=mecanismo
    }
    fun activar(){
        if(!this.mecanismo?.estaAbierto!!){
            mecanismo!!.open()
            println("Accede usuario")
            mecanismo!!.close()
        }
    }
}
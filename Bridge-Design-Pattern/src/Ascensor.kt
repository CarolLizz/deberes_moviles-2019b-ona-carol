class Ascensor: Mecanismo {
    override var estaAbierto: Boolean=false
    override fun open() {
        println("Puertas del ascensor abiertas")
        this.estaAbierto=true
    }

    override fun close() {
        println("Puertas del ascensor cerradas")
        this.estaAbierto=false
    }

}
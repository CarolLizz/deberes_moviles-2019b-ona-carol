class Puerta:Mecanismo{
    override var estaAbierto: Boolean=false

    override fun open() {
        println("Puerta abierta")
        this.estaAbierto=true
    }

    override fun close() {
       println("Puerta cerrada")
        this.estaAbierto=false
    }

}
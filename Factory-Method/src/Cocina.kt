class Cocina(marca:String,quemadores:Long) : Electrodomestico  {



    override fun encender() {
        println("Cocina encendida")
    }

    override fun apagar() {
        println("Cocina apagada")
    }

    override fun run() {
        println("Cocinando productos")
    }
}
class FabricaElectrodomesticos {
    fun  fabricarElectrodomestico(tipo:String):Electrodomestico{
        return when (tipo) {
            "cocina" -> Cocina("Mabe", 6)
            "lavadora" -> Lavadora(25, "blanco")
            else -> ElectrodomesticoDesconocido()
        }

    }
}
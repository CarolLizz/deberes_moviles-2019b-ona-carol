class ElectrodomesticoDesconocido:Electrodomestico {

    override fun run() {
        println("No puede correr un electrodomestico desconocido")
    }

    override fun encender() {
        println("No puede encender un electrodomestico desconocido")

    }

    override fun apagar() {
        println("No puede apagar un electrodomestico desconocido")

    }
}
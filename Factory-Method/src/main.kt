fun main(args: Array<String>){
    val fabrica = FabricaElectrodomesticos()
    val electrodomestico1 = fabrica.fabricarElectrodomestico("cocina")
    electrodomestico1.encender()
    electrodomestico1.run()
    electrodomestico1.apagar()
}
class Proyecto {
    lateinit var estado:Estado

    constructor(){
        estado=Nuevo(this)
    }
    fun cambiarEstado(estado: Estado){
        this.estado=estado
    }

    fun compilar(){
        this.estado.compilar()
    }

    fun probar(){
        this.estado.probar()
    }

    fun desplegar(){
        this.estado.desplegar()
    }


}
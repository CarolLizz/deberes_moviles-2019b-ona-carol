class Nuevo(override var proyecto: Proyecto) :Estado {

    override fun compilar() {
        proyecto.cambiarEstado(Compilado(proyecto))
        println("Proyecto compilado exitosamente")
    }

    override fun probar() {
        println("No se puede probar un proyecto sin compilar")
    }

    override fun desplegar() {
        println("No se puede desplegar un proyecto sin compilar")
    }
}
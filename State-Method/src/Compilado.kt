class Compilado(override var proyecto: Proyecto) :Estado {

    override fun compilar() {
        println("El proyecto ya se encuentra compilado")
    }

    override fun probar() {
        proyecto.cambiarEstado(Probado(proyecto))
        println("Proyecto probado exitosamente")
    }

    override fun desplegar() {
        println("El proyecto no puede ser desplegado sin antes probarse")
    }
}
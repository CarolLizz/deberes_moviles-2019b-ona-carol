class Desplegado(override var proyecto: Proyecto) :Estado {

    override fun compilar() {
        proyecto.cambiarEstado(Desplegado(proyecto))
        println("Proyecto compilado exitosamente")
    }

    override fun probar() {
        proyecto.cambiarEstado(Probado(proyecto))
        println("Proyecto probado exitosamente")
    }

    override fun desplegar() {
        println("El proyecto ya se encuentra desplegado")
    }
}
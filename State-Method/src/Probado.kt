class Probado(override var proyecto: Proyecto) : Estado {

    override fun compilar() {
        proyecto.cambiarEstado(Compilado(proyecto))
        println("Proyecto compilado exitosamente")
    }

    override fun probar() {
        println("El proyecto ya se encuentra probado")
    }

    override fun desplegar() {
        proyecto.cambiarEstado(Desplegado(proyecto))
        println("Proycto desplegado exitosamente")
    }
}